package com.concurrency.quiz.service;

import com.concurrency.quiz.event.Event;

import java.util.function.Consumer;

public interface CardAssigner {

    void assignCard(long userId, long cardId);

    void subscribe(Consumer<Event> consumer);
}
