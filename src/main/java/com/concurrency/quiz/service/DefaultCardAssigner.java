package com.concurrency.quiz.service;

import com.concurrency.quiz.event.Event;
import java.util.function.Consumer;

public class DefaultCardAssigner implements CardAssigner {

    public DefaultCardAssigner(ConfigurationProvider configurationProvider) {
    }

    @Override
    public void assignCard(long userId, long cardId) {

    }

    @Override
    public void subscribe(Consumer<Event> consumer) {

    }
}
