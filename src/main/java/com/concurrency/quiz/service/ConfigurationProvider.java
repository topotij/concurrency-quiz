package com.concurrency.quiz.service;

import com.concurrency.quiz.domain.Album;

public interface ConfigurationProvider {

    Album get();

}
